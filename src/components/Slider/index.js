import { useEffect } from 'react';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';

import './index.css';

const Slider = () => {
    let arrSliderImages = [
        'img/vendor-1.jpg',
        'img/vendor-2.jpg',
        'img/vendor-3.jpg',
        'img/vendor-4.jpg',
        'img/vendor-5.jpg',
        'img/vendor-6.jpg',
        'img/vendor-7.jpg',
        'img/vendor-8.jpg',
    ];

    return (
        <div className='container-fluid py-5'>
            <div className='row px-xl-5'>
                <div className='col'>
                    <OwlCarousel className='owl-theme' loop margin={29} nav={false} autoplay smartSpeed={1000} items={6} dots={false}>
                        {arrSliderImages.map((imageSrc, index) => (
                            <div className='bg-light p-4' key={index}>
                                <img src={imageSrc} alt='' />
                            </div>
                        ))}
                    </OwlCarousel>
                </div>
            </div>
        </div>
    );
};

export default Slider;