import { useEffect } from 'react';
import WebFont from 'webfontloader';

import { library } from '@fortawesome/fontawesome-svg-core';
import { 
  faLocationDot,
  faEnvelope,
  faPhone,
  faAngleRight,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import './index.css';

library.add(
  faLocationDot,
  faEnvelope,
  faPhone,
  faAngleRight,
);

const Footer = () => {
  useEffect(() => {
    WebFont.load({
      google: {
        families: ['Roboto:400']
      }
    });
  }, []);

  return (
    <div className="container-fluid bg-dark text-secondary footer mt-5 pt-5">
      <div className="row px-xl-5 pt-5">
          <div className="col-lg-4 col-md-12 mb-5 pr-3 pr-xl-5">
              <h5 className="text-secondary ff-Roboto text-uppercase mb-4">Get In Touch</h5>
              <p className="mb-4 ff-Roboto">
                No dolore ipsum accusam no lorem. Invidunt sed clita kasd clita et et dolor sed dolor. Rebum tempor no vero est magna amet no
              </p>
              <p className="mb-2 ff-Roboto">
                <FontAwesomeIcon icon={"fa-location-dot"} className='me-3' color='#ffd333' />
                123 Street, New York, USA
              </p>
              <p className="mb-2 ff-Roboto">
                <FontAwesomeIcon icon={"fa-envelope"} className='me-3' color='#ffd333' />
                info@example.com
              </p>
              <p className="mb-0 ff-Roboto">
                <FontAwesomeIcon icon={"fa-phone"} className='me-3' color='#ffd333' />
                +012 345 67890
              </p>
          </div>
          <div className="col-lg-8 col-md-12">
              <div className="row">
                  <div className="col-md-4 mb-5">
                      <h5 className="text-secondary text-uppercase mb-4 ff-Roboto">Quick Shop</h5>
                      <div className="d-flex flex-column justify-content-start">
                          <a className="text-secondary mb-2 ff-Roboto" href="#">
                            <FontAwesomeIcon icon={"fa-angle-right"} className='me-2' color='#ffffff' />
                            Home
                          </a>
                          <a className="text-secondary mb-2 ff-Roboto" href="#">
                            <FontAwesomeIcon icon={"fa-angle-right"} className='me-2' color='#ffffff' />
                            Our Shop
                          </a>
                          <a className="text-secondary mb-2 ff-Roboto" href="#">
                            <FontAwesomeIcon icon={"fa-angle-right"} className='me-2' color='#ffffff' />
                            Shop Detail
                          </a>
                          <a className="text-secondary mb-2 ff-Roboto" href="#">
                            <FontAwesomeIcon icon={"fa-angle-right"} className='me-2' color='#ffffff' />
                            Shopping Cart
                          </a>
                          <a className="text-secondary mb-2 ff-Roboto" href="#">
                            <FontAwesomeIcon icon={"fa-angle-right"} className='me-2' color='#ffffff' />
                            Checkout
                          </a>
                          <a className="text-secondary ff-Roboto" href="#">
                            <FontAwesomeIcon icon={"fa-angle-right"} className='me-2' color='#ffffff' />
                            Contact Us
                          </a>
                      </div>
                  </div>
                  <div className="col-md-4 mb-5">
                      <h5 className="text-secondary text-uppercase mb-4 ff-Roboto">My Account</h5>
                      <div className="d-flex flex-column justify-content-start">
                      <a className="text-secondary mb-2 ff-Roboto" href="#">
                            <FontAwesomeIcon icon={"fa-angle-right"} className='me-2' color='#ffffff' />
                            Home
                          </a>
                          <a className="text-secondary mb-2 ff-Roboto" href="#">
                            <FontAwesomeIcon icon={"fa-angle-right"} className='me-2' color='#ffffff' />
                            Our Shop
                          </a>
                          <a className="text-secondary mb-2 ff-Roboto" href="#">
                            <FontAwesomeIcon icon={"fa-angle-right"} className='me-2' color='#ffffff' />
                            Shop Detail
                          </a>
                          <a className="text-secondary mb-2 ff-Roboto" href="#">
                            <FontAwesomeIcon icon={"fa-angle-right"} className='me-2' color='#ffffff' />
                            Shopping Cart
                          </a>
                          <a className="text-secondary mb-2 ff-Roboto" href="#">
                            <FontAwesomeIcon icon={"fa-angle-right"} className='me-2' color='#ffffff' />
                            Checkout
                          </a>
                          <a className="text-secondary ff-Roboto" href="#">
                            <FontAwesomeIcon icon={"fa-angle-right"} className='me-2' color='#ffffff' />
                            Contact Us
                          </a>
                      </div>
                  </div>
                  <div className="col-md-4 mb-5">
                      <h5 className="text-secondary text-uppercase mb-4 ff-Roboto">Newsletter</h5>
                      <p className="ff-Roboto">Duo stet tempor ipsum sit amet magna ipsum tempor est</p>
                      <form action="">
                          <div className="input-group">
                              <input type="text" className="form-control" placeholder="Your Email Address" />
                              <div className="input-group-append">
                                  <button className="btn btn-primary ff-Roboto">Sign Up</button>
                              </div>
                          </div>
                      </form>
                      <h6 className="text-secondary text-uppercase mt-4 mb-3 ff-Roboto">Follow Us</h6>
                      <div className="d-flex">
                          <a className="btn btn-primary btn-square mr-2" href="#"><i className="fab fa-twitter"></i></a>
                          <a className="btn btn-primary btn-square mr-2" href="#"><i className="fab fa-facebook-f"></i></a>
                          <a className="btn btn-primary btn-square mr-2" href="#"><i className="fab fa-linkedin-in"></i></a>
                          <a className="btn btn-primary btn-square" href="#"><i className="fab fa-instagram"></i></a>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <div className="row border-top mx-xl-5 py-4" style={{borderColor: "rgba(256, 256, 256, .1) !important"}}>
          <div className="col-md-6 px-xl-0">
              <p className="mb-md-0 text-center text-md-left text-secondary">
                  &copy; <a className="text-primary ff-Roboto" href="#">Domain</a>. All Rights Reserved. Designed
                  by
                  <a className="text-primary ff-Roboto" href="https://htmlcodex.com">HTML Codex</a>
              </p>
          </div>
          <div className="col-md-6 px-xl-0 text-center text-md-right">
              <img className="img-fluid" src="img/payments.png" alt="" />
          </div>
      </div>
    </div>
  );
};

export default Footer;