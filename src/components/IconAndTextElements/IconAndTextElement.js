import { useEffect } from 'react';
import WebFont from 'webfontloader';

import { library } from '@fortawesome/fontawesome-svg-core';
import { 
  faCheck,
  faShippingFast,
  faExchangeAlt,
  faPhoneVolume
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

library.add(
  faCheck,
  faShippingFast,
  faExchangeAlt,
  faPhoneVolume,
);

const IconAndTextElement = ({icon, text}) => {
  useEffect(() => {
    WebFont.load({
      google: {
        families: ['Roboto:400', 'Roboto:500', 'Roboto:700']
      }
    });
  }, []);

  return (
    <div className='col-lg-3 col-md-6 col-sm-12 pb-1'>
      <div className='d-flex align-items-center bg-light mb-4 p-30'>
        <FontAwesomeIcon icon={icon} className='fs-40' color='#ffd333' />
        <span className='ff-Roboto title'>
          {text}
        </span>
      </div>
    </div>
  );
}

export default IconAndTextElement;