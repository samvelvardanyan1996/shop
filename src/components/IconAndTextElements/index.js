import IconAndTextElement from './IconAndTextElement';

import './index.css';

const IconAndTextElements = () => {
  let elements = [
    {
      icon: 'fa-check',
      text: 'Quality Product'
    },
    {
      icon: 'fa-shipping-fast',
      text: 'Free Shipping'
    },
    {
      icon: 'fa-exchange-alt',
      text: '14-Day Return'
    },
    {
      icon: 'fa-phone-volume',
      text: '24/7 Support'
    },
  ];

  return (
    <div id='app'>
      <div className='container-fluid pt-5'>
        <div className='row px-xl-5 pb-3'>
          {elements.map((element, index) => (
            <IconAndTextElement icon={element.icon} text={element.text} key={index} />
          ))}
        </div>
      </div>
    </div>
  );
};

export default IconAndTextElements;