import { useEffect } from 'react';
import WebFont from 'webfontloader';

import "./index.css";

const SpecialOffers = () => {
    useEffect(() => {
        WebFont.load({
            google: {
                families: ['Roboto:500', 'Roboto:700']
            }
        });
    }, []);
      
    let arrSpecialOffers = [
        'img/offer-1.jpg',
        'img/offer-2.jpg',
    ];

    return (
        <div className='container-fluid pt-5 pb-3'>
            <div className='row px-xl-5'>
                {arrSpecialOffers.map((imageSrc, index) => (
                    <div className='col-md-6' key={index}>
                        <div className='product-offer mb-30'>
                            <img className='img-fluid' src={imageSrc} alt='' />
                            <div className='offer-text'>
                                <h6 className='ff-Roboto text-white text-uppercase'>Save 20%</h6>
                                <h3 className='ff-Roboto text-white mb-3'>Special Offer</h3>
                                <a href='' className='btn btn-primary'>Shop Now</a>
                            </div>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
};

export default SpecialOffers;