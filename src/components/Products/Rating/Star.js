import { library } from '@fortawesome/fontawesome-svg-core';
import { faStar } from '@fortawesome/free-solid-svg-icons';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

library.add(faStar);

const Star = ({icon}) => {
    return (
        <small className='fa fa-star text-primary me-1'>
            <FontAwesomeIcon icon={icon} className='fs-16' color='#ffd333' />
        </small>
    );
}

export default Star;