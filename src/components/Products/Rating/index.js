import { library } from '@fortawesome/fontawesome-svg-core';
import { faStar, faStarHalfStroke } from '@fortawesome/free-solid-svg-icons';
import { faStar as faRegularStar} from '@fortawesome/free-regular-svg-icons';

import Star from './Star';

library.add(faStar, faRegularStar);

const Rating = ({count_star, number}) => {
    let ratingNumber = count_star;

    return (
        <div className='d-flex align-items-center justify-content-center mb-1'>
            {[...(new Array(5))].map((star, index) => {
                let iconName = faStar;
                if(index > ratingNumber - 1) {
                    if((index - ratingNumber > -1) && (index - ratingNumber < 0)) {
                        iconName = faStarHalfStroke;
                    }
                    else {
                        iconName = faRegularStar;
                    }
                }

                return <Star icon={iconName} key={index} />;
            })}
            <small>({number})</small>
        </div>
    );
}

export default Rating;