import Product from './Product';

import "./index.css";

const Products = ({title}) => {
  let ProductsArr = [
    {name: 'Product Name Goes Here', current_price: 123, old_price: 129, text: '100 Products', count_star: 5,   number: 99, src: '/img/product-1.jpg'},
    {name: 'Product Name Goes Here', current_price: 123, old_price: 129, text: '100 Products', count_star: 4.5, number: 99, src: '/img/product-2.jpg'},
    {name: 'Product Name Goes Here', current_price: 123, old_price: 129, text: '100 Products', count_star: 3.5, number: 99, src: '/img/product-3.jpg'},
    {name: 'Product Name Goes Here', current_price: 123, old_price: 129, text: '100 Products', count_star: 3,   number: 99, src: '/img/product-4.jpg'},
    {name: 'Product Name Goes Here', current_price: 123, old_price: 129, text: '100 Products', count_star: 5,   number: 99, src: '/img/product-5.jpg'},
    {name: 'Product Name Goes Here', current_price: 123, old_price: 129, text: '100 Products', count_star: 4.5, number: 99, src: '/img/product-6.jpg'},
    {name: 'Product Name Goes Here', current_price: 123, old_price: 129, text: '100 Products', count_star: 3.5, number: 99, src: '/img/product-7.jpg'},
    {name: 'Product Name Goes Here', current_price: 123, old_price: 129, text: '100 Products', count_star: 3,   number: 99, src: '/img/product-8.jpg'},
  ];

  return (
    <div className='container-fluid pt-5'>
      <h2 className='section-title position-relative text-uppercase mx-xl-5 mb-4'>
        <span className='ff-Roboto bg-secondary pe-3'>{title}</span>
      </h2>
      <div className='row px-xl-5'>
        {ProductsArr.map((product, index) =>
          <Product
            src={product.src}
            name={product.name}
            current_price={product.current_price}
            old_price={product.old_price}
            text={product.text}
            count_star={product.count_star}
            number={product.number}
            key={index}
          />
        )}
      </div>
    </div>
  );
}

export default Products;