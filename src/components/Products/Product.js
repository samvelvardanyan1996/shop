import { useEffect } from 'react';
import WebFont from 'webfontloader';

import { library } from '@fortawesome/fontawesome-svg-core';
import { 
    faCartShopping,
    faRotate,
    faSearch,
    faStar,
} from '@fortawesome/free-solid-svg-icons';
import { 
    faHeart,
} from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import Rating from './Rating';

library.add(
    faCartShopping,
    faHeart,
    faRotate,
    faSearch,
    faStar,
);

const Product = ({src, name, current_price, old_price, number, count_star}) => {
    useEffect(() => {
        WebFont.load({
            google: {
                families: ['Roboto:500']
            }
        });
    }, []);

    return (
        <div className='col-lg-3 col-md-4 col-sm-6 pb-1'>
            <div className='product-item bg-light mb-4'>
                <div className='product-img position-relative overflow-hidden'>
                    <img className='img-fluid w-100' src={src} alt='' />
                    <div className='product-action'>
                        <a className='btn btn-outline-dark btn-square d-flex align-items-center justify-content-center' href=''>
                            <FontAwesomeIcon icon='fa-cart-shopping' className='fs-16' />
                        </a>
                        <a className='btn btn-outline-dark btn-square' href=''>
                            <FontAwesomeIcon icon='fa-regular fa-heart' className='fs-16' />
                        </a>
                        <a className='btn btn-outline-dark btn-square' href=''>
                            <FontAwesomeIcon icon='fa-rotate' className='fs-16' />
                        </a>
                        <a className='btn btn-outline-dark btn-square' href=''>
                            <FontAwesomeIcon icon='fa-search' className='fs-16' />
                        </a>
                    </div>
                </div>
                <div className='text-center py-4'>
                    <a className='h6 text-decoration-none text-truncate' href=''>{name}</a>
                    <div className='d-flex align-items-center justify-content-center mt-2'>
                        <h5 className='ff-Roboto'>${current_price}</h5>
                        <h6 className='text-muted ms-2'><del>${old_price}</del></h6>
                    </div>
                    <div className='d-flex align-items-center justify-content-center mb-1'>
                        <Rating number={number} count_star={count_star} />
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Product;