import './index.css';

const Category = ({src, name, text}) => {
    return (
        <div className='col-lg-3 col-md-4 col-sm-6 pb-1'>
            <a className='text-decoration-none' href=''>
                <div className='cat-item d-flex align-items-center mb-4'>
                    <div className='overflow-hidden' style={{width: 100, height: 100}}>
                        <img className='img-fluid' src={src} alt='' />
                    </div>
                    <div className='flex-fill ps-3'>
                        <h6>{name}</h6>
                        <small className='text-body'>{text}</small>
                    </div>
                </div>
            </a>
        </div>
    );
}

export default Category;