import { useEffect } from 'react';
import WebFont from 'webfontloader';

import Category from './Category';

const Categories = ({title}) => {
  useEffect(() => {
    WebFont.load({
      google: {
        families: ['Roboto:400']
      }
    });
  }, []);

  let categoryArr = [
    {name: 'Category Name', text: '100 Products', src: '/img/girl.jpg'},
    {name: 'Category Name', text: '100 Products', src: '/img/camera.jpg'},
    {name: 'Category Name', text: '100 Products', src: '/img/sneakers.jpg'},
    {name: 'Category Name', text: '100 Products', src: '/img/curology.jpg'},
    {name: 'Category Name', text: '100 Products', src: '/img/curology.jpg'},
    {name: 'Category Name', text: '100 Products', src: '/img/sneakers.jpg'},
    {name: 'Category Name', text: '100 Products', src: '/img/camera.jpg'},
    {name: 'Category Name', text: '100 Products', src: '/img/girl.jpg'},
    {name: 'Category Name', text: '100 Products', src: '/img/camera.jpg'},
    {name: 'Category Name', text: '100 Products', src: '/img/girl.jpg'},
    {name: 'Category Name', text: '100 Products', src: '/img/curology.jpg'},
    {name: 'Category Name', text: '100 Products', src: '/img/sneakers.jpg'}
  ];

  return (
    <div className='container-fluid pt-5'>
      <h2 className='section-title position-relative text-uppercase mx-xl-5 mb-4'>
        <span className='ff-Roboto bg-secondary pe-3'>{title}</span>
      </h2>
      <div className='row px-xl-5 pb-3'>
        {categoryArr.map((category, index) =>
          <Category src={category.src} name={category.name} text={category.text} key={index} />
        )}
      </div>
    </div>
  );
}

export default Categories;