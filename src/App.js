import IconAndTextElements from './components/IconAndTextElements';
import Categories from './components/Categories';
import Products from './components/Products';
import SpecialOffers from './components/SpecialOffers';
import Slider from './components/Slider';
import Footer from './components/Footer';

import 'bootstrap/dist/css/bootstrap.min.css';

const App = () => {
  return (
    <div id='app'>
      <IconAndTextElements />
      <Categories title={"Categories"} />
      <Products title={"Featured Products"} />
      <SpecialOffers />
      <Products title={"Recent Products"} />
      <Slider />
      <Footer />
    </div>
  );
};

export default App;